# Option type for C\# 

Reference type was *heavily* influenced by 
[Scala's `Option`](https://github.com/scala/scala/blob/master/src/library/scala/Option.scala), but uses C# value type semantics.

##### Equality

The `Option<T>` struct uses structural equality and implements `IEquatable<Option<T>>`. Two options are the same if:

* Both options hold a value and both options hold the same value (as determined by `EqualityComparer<T>.Default.Equals`).

**or**

* Both options are `None`.

Operators `==` and `!=` are also overloaded.

##### ToString() representation
Default string representation is either the `value.ToString()` if the option has a value or `"None"` string literal.

## Examples

#### Creating an option from anything:

	MyType obj;

	//null or not, doesn't matter:
    Option<MyType> option = obj.ToOption();
	//or
	var option = Option.From(obj);

It will create either non-empty `Option` if obj was not null or an empty `Option` if it was.


An implicit conversion to `Option` also exists:
    
    Option<int> option = 4; // option with value 4
	Option<MyRefType> maybeObj = null // None


Creating `None`/empty `Option`:	

		None<int> noInt = Option<int>.None;

Creating `Some`/non-empty `Option`:
	
		Option<int> someInt = Option<int>.Some(4);
		//equivalent with type inference:
		var someInt = Option.Some(4);

Creating option with `Some` will cause `ArgumentNullException` to be thrown if the wrapped value is `null`.

#### Using `OrElse`:

	var stringList = new List<string> { "Just a test", null, "Another nice string", "And just one more", null };

	var replacedNulls = stringList.Select(x => x.OrElse(() => stringList.FirstOrDefault().ToOption()))
    .SelectMany(x => x.ToEnumerable());

	//Output: "Just a test", "Just a test", "Another nice string", "And just one more", "Just a test"

Note that `OrElse` has two overloads, of which the one taking `Func<Option<T>>` is the preferred one.

#### Using `GetOrElse` / `GetOrDefault`:
	
	var optionalDecimal = Option<decimal>.None;
	var dividedByTwo = optionalDecimal.Select(x => x/2).GetOrElse(140.50m);
	//Output: dividedByTwo = 140.50
	
`GetOrElse` also has two overloads, similar to `OrElse`: one taking `T`, other one: `Func<T>`. Depending
on the context it might be better to use one or the other.

`GetOrDefault` returns the wrapped value or the `default(T)` value if the option is `None`.

##### Using `GetOrElseAsync`:
When materializing the value, a `Task` can be returned by `GetOrElseAsync`:

    var optionalLabel = Option<string>.None;
    var label = await optionalLabel.GetOrElseAsync(() => externalService.FetchAsync());

#### Using `ToEnumerable()`:
`Option` can be converted to enumerable by calling `Option<T>.ToEnumerable()`. Resulting sequence can have either 0 elements if the option is `None`, 1 element otherwise.

### Query syntax

Query syntax for corresponding methods should work. One additional extension method that was added was the overload for `SelectMany`.

    var first = Option.Some(5);
    var second = Option<int>.None;
	var third = Option.Some(8);

    var firstResult = from x in first
				 	  from y in second
				 	  let r = x + y
				 	  where r > 10
				 	  select r

	// 'firstResult' will be None

    var secondResult = from x in first
				 	   from y in third
				 	   let r = x + y
				 	   where r > 10
				 	   select r

    // 'secondResult' will be Some(13)

	
## Extensions

The root namespace for Option-related extensions is `Optional.Extensions`.

### `Optional.Extensions.Collections` namespace

##### `IList<T>`

* `GetAt`: alternative to [the indexer `get` method](http://msdn.microsoft.com/en-us/library/ewthkb10%28v=vs.110%29.aspx). Returns an `Option` by getting the value from the list at the given index. If the value is `null`, the list was empty or the index was out of bounds, `None` is returned.


##### `IDictionary<TKey, TValue>`

* `GetValue`: Alternative to [`IDictionary<TKey, TValue>.TryGetValue`](http://msdn.microsoft.com/en-us/library/bb299639%28v=vs.110%29.aspx). If the key is not present in the dictionary or the value is `null`, `None` is returned.

### `Optional.Extensions.Enumerable` namespace

##### `IEnumerable<T>`

* `FirstOption()`: Alternative to [`IEnumerable<T>.FirstOrDefault()`](http://msdn.microsoft.com/en-us/library/bb340482%28v=vs.110%29.aspx).
* `FirstOption(Func<T,bool>)`: Alternative to [`IEnumerable<T>.FirstOrDefault(Func<T,bool>)`](http://msdn.microsoft.com/en-us/library/bb549039%28v=vs.110%29.aspx).
* `LastOption()`: Alternative to [`IEnumerable<T>.LastOrDefault()`](http://msdn.microsoft.com/en-us/library/bb301849%28v=vs.110%29.aspx).
* `LastOption(Func<T,bool>)`: Alternative to [`IEnumerable<T>.LastOrDefault(Func<T,bool>)`](http://msdn.microsoft.com/en-us/library/bb548915%28v=vs.110%29.aspx).


## Parsing utilities
Static class `Parse` (in `Optional.Parsing` namespace) holds a number of methods that
can substitute the `TryParse` for most of value types that provide such functionality (e.g. `Int32`, `Int16`, `Byte`, `DateTime` etc.).



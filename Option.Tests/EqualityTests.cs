﻿namespace Option.Tests
{
    using NUnit.Framework;
    using Optional;

    [TestFixture]
    public class EqualityTests
    {
        [Test]
        public void TwoOptionsAreEqualIfBothHoldValueAndTheValueIsEqual()
        {
            var first = Option.From("abc");
            var second = Option.From("def");
            var third = Option<string>.None;

            Assert.AreNotEqual(first, second);
            Assert.AreNotEqual(first, third);
            Assert.AreNotEqual(second, third);
            Assert.AreEqual(first, Option.From("abc"));
        }

        [Test]
        public void EqualityOperatorIsOverloaded()
        {
            var first = Option.From("abc");
            var second = Option.From("def");
            var third = Option<string>.None;
            Assert.IsFalse(first == second);
            Assert.IsFalse(first == third);
            Assert.IsFalse(second == third);
            Assert.IsTrue(first == Option.From("abc"));
        }

        [Test]
        public void InequalityOperatorIsOverloaded()
        {
            var first = Option.From("abc");
            var second = Option.From("def");
            var third = Option<string>.None;
            Assert.IsTrue(first != second);
            Assert.IsTrue(first != third);
            Assert.IsTrue(second != third);
            Assert.IsFalse(first != Option.From("abc"));
        }
    }
}
﻿namespace Optional.Parsing
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Provides methods for parsing strings to <see cref="T:Optional.Option`1"/> type.
    /// <remarks>All of the methods are functional equivalents of TryParse</remarks>
    /// </summary>
    public static class Parse
    {
        /// <summary>
        /// Converts the string representation to its 16-bit signed integer equivalent.
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int16> Int16(string input)
        {
            Int16 value;
            return System.Int16.TryParse(input, out value) ? Option.Some(value) : Option<Int16>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 16-bit signed integer equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int16> Int16(string input, NumberStyles style, IFormatProvider provider)
        {
            Int16 value;
            return System.Int16.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Int16>.None;
        }

        /// <summary>
        /// Converts the string representation to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int32> Int32(string input)
        {
            Int32 value;
            return System.Int32.TryParse(input, out value) ? Option.Some(value) : Option<Int32>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 32-bit signed integer equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int32> Int32(string input, NumberStyles style, IFormatProvider provider)
        {
            Int32 value;
            return System.Int32.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Int32>.None;
        }

        /// <summary>
        /// Converts the string representation to its 64-bit signed integer equivalent.
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int64> Int64(string input)
        {
            Int64 value;
            return System.Int64.TryParse(input, out value) ? Option.Some(value) : Option<Int64>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 64-bit signed integer equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Int64> Int64(string input, NumberStyles style, IFormatProvider provider)
        {
            Int64 value;
            return System.Int64.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Int64>.None;
        }

        /// <summary>
        /// Tries to convert the specified string representation of a logical value to its <see cref="System.Boolean"/> equivalent. 
        /// </summary>
        /// <param name="input">A string containing the value to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<bool> Boolean(string input)
        {
            Boolean value;
            return System.Boolean.TryParse(input, out value) ? Option.Some(value) : Option<Boolean>.None;
        }

        /// <summary>
        /// Converts the string representation of a number to its single-precision floating-point number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Single> Single(string input)
        {
            Single value;
            return System.Single.TryParse(input, out value) ? Option.Some(value) : Option<Single>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its single-precision floating-point number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Single> Single(string input, NumberStyles style, IFormatProvider provider)
        {
            Single value;
            return System.Single.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Single>.None;
        }

        /// <summary>
        /// Converts the string representation of a number to its double-precision floating-point number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Double> Double(string input)
        {
            Double value;
            return System.Double.TryParse(input, out value) ? Option.Some(value) : Option<Double>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its double-precision floating-point number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Double> Double(string input, NumberStyles style, IFormatProvider provider)
        {
            Double value;
            return System.Double.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Double>.None;
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its Byte equivalent.
        /// </summary>
        /// <param name="input">A string that contains a number to convert. The string is interpreted using the <see cref="System.Globalization.NumberStyles.Integer"/> style.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Byte> Byte(string input)
        {
            Byte value;
            return System.Byte.TryParse(input, out value) ? Option.Some(value) : Option<Byte>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its <see cref="System.Byte"/> equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Byte> Byte(string input, NumberStyles style, IFormatProvider provider)
        {
            Byte value;
            return System.Byte.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<Byte>.None;
        }

        /// <summary>
        /// Converts the value of the specified string to its equivalent Unicode character. 
        /// </summary>
        /// <param name="input">A string that contains a single character, or null.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Char> Char(string input)
        {
            Char value;
            return System.Char.TryParse(input, out value) ? Option.Some(value) : Option<Char>.None;
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its <see cref="System.SByte"/> equivalent.
        /// </summary>
        /// <param name="input">A string that contains a number to convert.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<SByte> SByte(string input)
        {
            SByte value;
            return System.SByte.TryParse(input, out value) ? Option.Some(value) : Option<SByte>.None;
        }

        /// <summary>
        /// Tries to convert the string representation of a number in a specified style and culture-specific format to its <see cref="System.SByte"/> equivalent.
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<SByte> SByte(string input, NumberStyles style, IFormatProvider provider)
        {
            SByte value;
            return System.SByte.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<SByte>.None;
        }

        /// <summary>
        /// Converts the string representation of a number to its 16-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt16> UInt16(string input)
        {
            UInt16 value;
            return System.UInt16.TryParse(input, out value) ? Option.Some(value) : Option<UInt16>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 16-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt16> UInt16(string input, NumberStyles style, IFormatProvider provider)
        {
            UInt16 value;
            return System.UInt16.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<UInt16>.None;
        }

        /// <summary>
        /// Converts the string representation of a number to its 32-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt32> UInt32(string input)
        {
            UInt32 value;
            return System.UInt32.TryParse(input, out value) ? Option.Some(value) : Option<UInt32>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 32-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt32> UInt32(string input, NumberStyles style, IFormatProvider provider)
        {
            UInt32 value;
            return System.UInt32.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<UInt32>.None;
        }

        /// <summary>
        /// Converts the string representation of a number to its 64-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt64> UInt64(string input)
        {
            UInt64 value;
            return System.UInt64.TryParse(input, out value) ? Option.Some(value) : Option<UInt64>.None;
        }

        /// <summary>
        /// Converts the string representation of a number in a specified style and culture-specific format to its 64-bit unsigned integer number equivalent. 
        /// </summary>
        /// <param name="input">A string containing a number to convert. The string is interpreted using the style specified by <paramref name="style"/>.</param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="input"/>.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<UInt64> UInt64(string input, NumberStyles style, IFormatProvider provider)
        {
            UInt64 value;
            return System.UInt64.TryParse(input, style, provider, out value) ? Option.Some(value) : Option<UInt64>.None;
        }

        /// <summary>
        /// Converts the String representation of a number to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Decimal> Decimal(string input)
        {
            Decimal value;
            return System.Decimal.TryParse(input, out value) ? Option.Some(value) : Option<Decimal>.None;
        }

        /// <summary>
        /// Converts the String representation of a number to its <see cref="System.Decimal"/> equivalent using the specified style and culture-specific format.
        /// </summary>
        /// <param name="input">A string containing a number to convert</param>
        /// <param name="style">A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the permitted format of <paramref name="input"/></param>
        /// <param name="provider">An <see cref="System.IFormatProvider"/> object that supplies culture-specific formatting information about <paramref name="input"/></param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<Decimal> Decimal(string input, NumberStyles style, IFormatProvider provider)
        {
            Decimal value;
            return System.Decimal.TryParse(input, style, provider, out value)
                ? Option.Some(value)
                : Option<Decimal>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTime"/> equivalent
        /// </summary>
        /// <param name="input">A string containing a date and time to convert. </param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTime> DateTime(string input)
        {
            DateTime value;
            return System.DateTime.TryParse(input, out value) ? Option.Some(value) : Option<DateTime>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTime"/> equivalent using the specified culture-specific format information and formatting style.
        /// </summary>
        /// <param name="input">A string containing a date and time to convert. </param>
        /// <param name="provider">An object that supplies culture-specific formatting information about s. </param>
        /// <param name="styles">A bitwise combination of enumeration values that defines how to interpret the parsed date in relation to the current time zone or the current date.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTime> DateTime(string input, IFormatProvider provider, DateTimeStyles styles)
        {
            DateTime value;
            return System.DateTime.TryParse(input, provider, styles, out value)
                ? Option.Some(value)
                : Option<DateTime>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTime"/> equivalent using the specified array of formats, culture-specific format information, and style.
        /// The format of the string representation must match at least one of the specified formats exactly. 
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert. </param>
        /// <param name="formats">An array of allowable formats of <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="input"/></param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the permitted format of <paramref name="input"/></param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTime> DateTimeExact(string input, string[] formats, IFormatProvider provider,
            DateTimeStyles style)
        {
            DateTime value;
            return System.DateTime.TryParseExact(input, formats, provider, style, out value)
                ? Option.Some(value)
                : Option<DateTime>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTime"/> equivalent using the specified format, culture-specific format information, and style.
        /// The format of the string representation must match the specified format exactly. 
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert. </param>
        /// <param name="format">The required format of <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="input"/></param>
        /// <param name="style">A bitwise combination of enumeration values that indicates the permitted format of <paramref name="input"/></param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTime> DateTimeExact(string input, string format, IFormatProvider provider,
            DateTimeStyles style)
        {
            DateTime value;
            return System.DateTime.TryParseExact(input, format, provider, style, out value)
                ? Option.Some(value)
                : Option<DateTime>.None;
        }

        /// <summary>
        /// Tries to converts a specified string representation of a date and time to its <see cref="System.DateTimeOffset"/> equivalent
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert.</param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTimeOffset> DateTimeOffset(string input)
        {
            DateTimeOffset value;
            return System.DateTimeOffset.TryParse(input, out value) ? Option.Some(value) : Option<DateTimeOffset>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTimeOffset"/> equivalent using the specified culture-specific format information and formatting style.
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert.</param>
        /// <param name="provider">An object that provides culture-specific formatting information about <paramref name="input"/></param>
        /// <param name="styles">A bitwise combination of enumeration values that indicates the permitted format of <paramref name="input"/>. </param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTimeOffset> DateTimeOffset(string input, IFormatProvider provider,
            DateTimeStyles styles)
        {
            DateTimeOffset value;
            return System.DateTimeOffset.TryParse(input, provider, styles, out value)
                ? Option.Some(value)
                : Option<DateTimeOffset>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTimeOffset"/> equivalent using the specified format, culture-specific format information, and style.
        /// The format of the string representation must match the specified format exactly. 
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert. </param>
        /// <param name="format">The required format of <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="input"/></param>
        /// <param name="styles">A bitwise combination of enumeration values that indicates the permitted format of <paramref name="input"/></param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTimeOffset> DateTimeOffsetExact(string input, string format, IFormatProvider provider,
            DateTimeStyles styles)
        {
            DateTimeOffset value;
            return System.DateTimeOffset.TryParseExact(input, format, provider, styles, out value)
                ? Option.Some(value)
                : Option<DateTimeOffset>.None;
        }

        /// <summary>
        /// Converts the specified string representation of a date and time to its <see cref="System.DateTimeOffset"/> equivalent using the specified array of formats, culture-specific format information, and style.
        /// The format of the string representation must match at least one of the specified formats exactly. 
        /// </summary>
        /// <param name="input">A string that contains a date and time to convert. </param>
        /// <param name="formats">An array of allowable formats of <paramref name="input"/></param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="input"/></param>
        /// <param name="styles">A bitwise combination of enumeration values that indicates the permitted format of <paramref name="input"/></param>
        /// <returns>None if the conversion failed, wrapped value otherwise</returns>
        public static Option<DateTimeOffset> DateTimeOffsetExact(string input, string[] formats,
            IFormatProvider provider, DateTimeStyles styles)
        {
            DateTimeOffset value;
            return System.DateTimeOffset.TryParseExact(input, formats, provider, styles, out value)
                ? Option.Some(value)
                : Option<DateTimeOffset>.None;
        }
    }
}
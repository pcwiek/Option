﻿namespace Option.Tests
{
    using System;
    using NUnit.Framework;
    using Optional;

    [TestFixture]
    public class CreationTests
    {
        [Test]
        public void CreatingEmptyOption()
        {
            var option = Option<string>.None;
            Assert.IsFalse(option.HasValue);
        }

        [Test]
        public void CreatingOptionWithSomeHelper()
        {
            Assert.DoesNotThrow(() => Option.Some("abc"));
            var result = Option.Some("abc");
            Assert.IsTrue(result.HasValue);
        }

        [Test]
        public void CreatingOptionWithSome()
        {
            Assert.DoesNotThrow(() => Option<string>.Some("abc"));
            var result = Option<string>.Some("abc");
            Assert.IsTrue(result.HasValue);
        }

        [Test]
        public void CreatingOptionWithSomeHelperThrowsExceptionIfValueIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => Option.Some((string) null));
        }

        [Test]
        public void CreatingOptionWithSomeThrowsExceptionIfValueIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => Option<string>.Some(null));
        }

        [Test]
        public void CreatingOptionWithImplicitConversion()
        {
            string test = null;
            Option<string> option = test;
            Assert.IsFalse(option.HasValue);
            option = "test";
            Assert.IsTrue(option.HasValue);
        }

        [Test]
        public void CreatingNonEmptyOptionWithFromHelper()
        {
            var option = Option.From("abc");
            Assert.IsTrue(option.HasValue);
        }

        [Test]
        public void CreatingEmptyOptionWithFromHelper()
        {
            var option = Option.From((string) null);
            Assert.IsFalse(option.HasValue);
        }

        [Test]
        public void CreatingNonEmptyOptionWithExtensionMethod()
        {
            var option = "def".ToOption();
            Assert.IsTrue(option.HasValue);
        }

        [Test]
        public void CreatingEmptyOptionWithExtensionMethod()
        {
            var option = ((string) null).ToOption();
            Assert.IsFalse(option.HasValue);
        }
    }
}